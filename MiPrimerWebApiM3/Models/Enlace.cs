﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPrimerWebApiM3.Models
{
    public class Enlace
    {
        public string Href { get; private set; }  // representa el link
        public string Rel { get; private set; }  // representa la descripcion
        public string Metodo { get; private set; } //representa el metodo http a solicitar

        public Enlace(string href, string rel, string metodo)
        {
            Href = href;
            Rel = rel;
            Metodo = metodo;
        }
    }
}
