﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiPrimerWebApiM3.Entities
{
    public class Libro
    {
        public int Id { get; set; }
        [Required]
        public string Titulo { get; set; }
        [Required]
        //clave Foranea Implicita por convencion
        public int AutorId { get; set; }
        //Propiedad de Navegacion
        public Autor Autor { get; set; }
    }
}
