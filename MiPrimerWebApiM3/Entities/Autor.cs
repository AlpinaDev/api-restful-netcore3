﻿using MiPrimerWebApiM3.Helpers;
using MiPrimerWebApiM3.Migrations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MiPrimerWebApiM3.Entities
{
    public class Autor : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!string.IsNullOrEmpty(Nombre))
            {
                var primeraLetra = Nombre[0].ToString();
                if (primeraLetra != primeraLetra.ToUpper())
                {
                    yield return new ValidationResult("La Primera letra debe ser Mayuscula", new string[] { nameof(Nombre) });
                }
            }
            
        }
        public int Id { get; set; }
        [Required]
        [PrimeraLetraMayuscula]
        [StringLength(10, ErrorMessage = "El campo Nombre debe tener {1} caracteres o menos")]
        public string Nombre { get; set; }
        [Range(18,120)] // La edad debe ser entre 18 y 120
        public int Edad { get; set; }
        [CreditCard]
        public string CreditCard { get; set; }
        [Url]
        public string Url { get; set; }
        //Propiedad de Navegacion
        public List<Libro> Libros { get; set; }

        
    }
}
