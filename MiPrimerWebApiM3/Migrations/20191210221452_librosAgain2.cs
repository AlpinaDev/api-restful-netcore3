﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MiPrimerWebApiM3.Migrations
{
    public partial class librosAgain2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Libors_Autores_AutorId",
                table: "Libors");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Libors",
                table: "Libors");

            migrationBuilder.RenameTable(
                name: "Libors",
                newName: "Libros");

            migrationBuilder.RenameIndex(
                name: "IX_Libors_AutorId",
                table: "Libros",
                newName: "IX_Libros_AutorId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Libros",
                table: "Libros",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Libros_Autores_AutorId",
                table: "Libros",
                column: "AutorId",
                principalTable: "Autores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Libros_Autores_AutorId",
                table: "Libros");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Libros",
                table: "Libros");

            migrationBuilder.RenameTable(
                name: "Libros",
                newName: "Libors");

            migrationBuilder.RenameIndex(
                name: "IX_Libros_AutorId",
                table: "Libors",
                newName: "IX_Libors_AutorId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Libors",
                table: "Libors",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Libors_Autores_AutorId",
                table: "Libors",
                column: "AutorId",
                principalTable: "Autores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
