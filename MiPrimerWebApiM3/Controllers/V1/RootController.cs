﻿using Microsoft.AspNetCore.Mvc;
using MiPrimerWebApiM3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiPrimerWebApiM3.Controllers
{
    [ApiController]
    [Route("api")]
    public class RootController : ControllerBase
    {
        private readonly IUrlHelper urlHelper;

        public RootController(IUrlHelper urlHelper)
        {
            this.urlHelper = urlHelper;
        }

        [HttpGet(Name = "GetRoot")]
        public ActionResult<IEnumerable<Enlace>> Get()
        {
            List<Enlace> enlaces = new List<Enlace>();
            //aqui colocamos los links
            enlaces.Add(new Enlace(href: urlHelper.Link("GetRoot", new { }), rel: "self", metodo: "GET"));
            //en el Link incluir el router o nombre de recurso. Ej:obtenerAUTORES  (el nombre de recurso es Autores)
            //rel representa una breve descripcion
            //metodo representa el metodo http
            enlaces.Add(new Enlace(href: urlHelper.Link("ObtenerAutores", new { }), rel: "autores", metodo: "GET"));
            enlaces.Add(new Enlace(href: urlHelper.Link("CrearAutor", new { }), rel: "crear-autor", metodo: "POST"));
            enlaces.Add(new Enlace(href: urlHelper.Link("ObtenerValores", new { }), rel: "valores", metodo: "GET"));
            return enlaces;
        }
    }
}
