﻿using Microsoft.AspNetCore.Mvc;
using MiPrimerWebApiM3.Contexts;
using MiPrimerWebApiM3.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace MiPrimerWebApiM3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibrosController : ControllerBase
    {
        public ApplicationDbContext Context { get; }
        //constructor
        public LibrosController(ApplicationDbContext context)
        {
            //inicializamos el context
            this.Context = context;
        }
        //2 o mas endpoints para el mismo metodo
        //metodo Get sin parametros
        [HttpGet]
        //endpoint: dominio.com/api/libros
        //REGLA DE RUTEO
        [HttpGet("/listado")] //template de ruta
        //endpoint: dominio.com/listado
        public ActionResult<IEnumerable<Libro>> Get()
        {
            //retornamos la entidad libros con el Autor(propiedad de navegacion)
            return this.Context.Libros.Include(x => x.Autor).ToList();
        }

        //metodo Get sin parametros con el nombre "primero"  incluido en el endpoint (REGLA DE RUTEO)
        //endpoint: api/Libros/primer
        //REGLA DE RUTEO
        [HttpGet("primero")]
        public ActionResult<Libro> GetPrimerLibro()
        {
            //retornamos el primer registro de la entidad autor con los Libros(propiedad de navegacion)
            return Context.Libros.Include(x => x.Autor).FirstOrDefault();
        }

        //agregamos un Parametro y nombre al metodo
        //endpoint: api/libros/9
        [HttpGet("{id}", Name = "obtenerLibro")]
        public ActionResult<Libro> Get(int id)
        {
            //filtramos por el parametro id
            var libro = Context.Libros.Where(x => x.Id == id).FirstOrDefault();
            if (libro == null)
            {
                //si no lo encuentra se retorna un NotFound
                return NotFound();

            }
            return libro;
        }

        [HttpPost]
        //FromBody indica que el autor esta cargado en el body
        //insertamos un recurso en la base de datos
        //endpoint: api/libros/9
        public ActionResult  Post([FromBody] Libro libro)
        {
            //en un POST insertamos
            Context.Libros.Add(libro);
            //guadamos los cambios
            Context.SaveChanges();
            // y retornamos un 201 created y en la cabecera, atributo "location" asignamos la url de consulta del recurso creado. "obtenerLibro" es el Route Name definido en el metodo Get con parametro id
            return new CreatedAtRouteResult("obtenerLibro", new { id = libro.Id }, libro);
        }

        //Put se utiliza para realizar Updates en la base de datos, pasamos el id como parametro
        [HttpPut("id")]
        //endpoint: api/libros/9
        //FromBody indica que el autor esta cargado en el body
        public ActionResult Put(int id, [FromBody] Libro libro)
        {
            if (id != libro.Id)
            {
                //si el Id que se pasa como párametro no es el mismo que el de la entidad se informa el error
                return BadRequest();
            }
            //indicamos al context que se esta modificando el recurso
            this.Context.Entry(libro).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            Context.SaveChanges();
            //retornamos un 200 ok
            return Ok();
        }

        [HttpDelete("id")]
        //endpoint: api/libros/9
        public ActionResult<Libro> Delete(int id)
        {
            var libro = Context.Libros.FirstOrDefault(x => x.Id == id);
            if (libro == null)
            {
                //si el recurso no existe en la base de datos informamos el error
                return NotFound();
            }

            Context.Libros.Remove(libro);
            Context.SaveChanges();
            //retornamos el recurso borrado
            return libro;
        }

    }
}
