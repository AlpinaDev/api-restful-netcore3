﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiPrimerWebApiM3.Contexts;
using MiPrimerWebApiM3.Entities;
using MiPrimerWebApiM3.Helpers;
using MiPrimerWebApiM3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MiPrimerWebApiM3.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AutoresController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        private readonly IUrlHelper urlHelper;

        public IMapper _mapper { get; }

        public AutoresController(ApplicationDbContext context, IMapper mapper,IUrlHelper urlHelper)
        {
            //inicializamos el contexto
            this.context = context;
            _mapper = mapper;
            this.urlHelper = urlHelper;
        }

        //2 o mas endpoints para el mismo metodo
        //metodo Get sin parametros
        [HttpGet(Name = "ObtenerAutores")]
        //endpoint: dominio.com/api/libros
        //REGLA DE RUTEO
        [HttpGet("/listado")] //template de ruta
                              /////////////////////////////////ASYC////////////////////////////////////
                              //ASYNC: marcamos el metodo como ASYNC, utilizamos el Tipo TASK, retornamos un AWAIT y utilizamos ToListAsync()
                              ////////public async Task<ActionResult<IEnumerable<Autor>>> Get()
                              ////////{
                              ////////    //retornamos la entidad autor con los Libros(propiedad de navegacion)
                              ////////    return await context.Autores.Include(x => x.Libros).ToListAsync();
                              ////////}
        public async Task<ActionResult<ColeccionDeRecursos<AutorDTO>>> Get()
        {
            //retornamos la entidad autor con los Libros(propiedad de navegacion)
            var autores = await context.Autores.Include(x => x.Libros).ToListAsync();
            //mapeamos los autores en autoresDTO porque AutorDTO hereda de la clase RECURSO, indispensable para poder llenar la coleccion de recursos
            var autoresDTO = _mapper.Map<List<AutorDTO>>(autores);
            autoresDTO.ForEach(a => GenerarEnlaces(a));
            //AutorDTO hereda de la clase RECURSO, indispensable para poder llenar la coleccion de recursos
            var resultado = new ColeccionDeRecursos<AutorDTO>(autoresDTO);
            resultado.Enlaces.Add(new Enlace(urlHelper.Link("obtenerAutores", new { }), rel: "self", metodo: "GET"));
            resultado.Enlaces.Add(new Enlace(urlHelper.Link("crearAutor", new { }), rel: "crear-autor", metodo: "POST"));

            return resultado; 
        }


        //metodo Get sin parametros con el nombre "primero"  incluido en el endpoint (REGLA DE RUTEO)
        //endpoint: api/autores/primero
        [ResponseCache(Duration = 15)]   //es un FILTRO de accion. Esta configurado en el archivo Startup.cs
        //[Authorize] //es un FILTRO de accion. Solo podran acceder al recurso las personas autenticadas a la api
        [HttpGet("primero")] //REGLA DE RUTEO
        [ServiceFilter(typeof(MiFiltroDeAccion))]  //FILTRO PERSONALIZADO solo utilizo "ServiceFilter"
        //porque tengo una inyeccion de "logger" como dependencia en el constructor de la clase "MiFiltroDeAccion"
        public ActionResult<Autor> GetPrimerAutor()
        {
            //retornamos el primer registro de la entidad autor con los Libros(propiedad de navegacion)
            return context.Autores.Include(x => x.Libros).FirstOrDefault();
        }

        //agregamos un Parametro y nombre al metodo
        //endpoint: api/autores/9 o api/autores/9/mensajeStringTest
        [HttpGet("{id}/{test?}", Name = "obtenerAutor")]  //en este caso se pasan 2 parametros. el simbolo "?" establece que es un parametro opcional
        //[HttpGet("{id}/{test=mensajeStringTest}", Name ="obtenerAutor")]  En este caso se establece un valor por default cuando no se pasa el valor como parametro.
        public ActionResult<AutorDTO> Get(int id, string test)
        {
            //retornamos la entidad autor con los Libros(propiedad de navegacion). Tambien filtramos por el parametro id
            var autor = context.Autores.Include(x => x.Libros).Where(x => x.Id == id).FirstOrDefault();
            if (autor == null)
            {
                //si no lo encuentra se retorna un NotFound
                return NotFound();

            }
            //estamos convirtiendo el objeto "autor" AutorDTO
            var autorDTO = this._mapper.Map<AutorDTO>(autor);
            GenerarEnlaces(autorDTO);

            // se retorna el recurso autorDTO
            return autorDTO;
        }

        private void GenerarEnlaces(AutorDTO autorDTO)
        {
            autorDTO.Enlaces.Add(new Enlace(href: urlHelper.Link("ObtenerAutores", new {id = autorDTO.Id }), rel: "autores", metodo: "GET"));
            autorDTO.Enlaces.Add(new Enlace(href: urlHelper.Link("ActualizarAutor", new { id = autorDTO.Id }), rel: "actualizar-autor", metodo: "PUT"));
            autorDTO.Enlaces.Add(new Enlace(href: urlHelper.Link("BorrarAutor", new { id = autorDTO.Id }), rel: "borrar-autor", metodo: "DELETE"));
        }

        [HttpPost(Name = "CrearAutor")]
        //FromBody indica que el autor esta cargado en el body
        //insertamos un recurso en la base de datos
        public ActionResult Post([FromBody] Autor autor)
        {
            context.Autores.Add(autor);
            context.SaveChanges();
            //retornamos un 201 created y en la cabecera, atributo "location" asignamos la url de consulta del recurso creado."obtenerAutor" es el Route Name definido en el metodo Get con parametro id
            return new CreatedAtRouteResult("obtenerAutor", new { id = autor.Id }, autor);
        }

        [HttpPost("asincronico")]
        public async Task<ActionResult> PostAsync([FromBody] AutorCreacionDTO autorCreacion)
        {
            //convertimos de AutoCreacionDTO a AUTOR
            var _autor = _mapper.Map<Autor>(autorCreacion);
            context.Add(_autor);
            await context.SaveChangesAsync();
            //convertimos de Autor a AutorDTO
            var autorDTO = _mapper.Map<AutorDTO>(_autor);
            //autorDTO es el objeto que se va a retornar en el body de la respuesta
            return new CreatedAtRouteResult("ObtenerAutor", new { id = _autor.Id }, autorDTO);
        }

        //Put se utiliza para realizar Updates en la base de datos, pasamos el id como parametro
        [HttpPut("{id}", Name = "ActualizarAutor")]
        //endpoint: api/autores/9
        //FromBody indica que el autor esta cargado en el body
        public ActionResult Put(int id, [FromBody] Autor autor)
        {
            if (id != autor.Id)
            {
                //si el Id que se pasa como párametro no es el mismo que el de la entidad se informa el error
                return BadRequest();
            }
            //indicamos al context que se esta modificando el recurso
            context.Entry(autor).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            //retornamos un 200 ok
            return Ok();
        }

        //Put se utiliza para realizar Updates en la base de datos, pasamos el id como parametro
        [HttpPut("{id}", Name = "update2")]
        //endpoint: api/autores/9
        //FromBody indica que el autor esta cargado en el body
        public async Task<ActionResult> Put2(int id, [FromBody] AutorCreacionDTO autorActualizacion)
        {
            //convertimos en Autor
            var autor = _mapper.Map<Autor>(autorActualizacion);
            autor.Id = id;
            //indicamos al context que se esta modificando el recurso
            context.Entry(autorActualizacion).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await context.SaveChangesAsync();
            //retornamos un 200 ok
            return NoContent();
        }
        [HttpDelete("{id}", Name = "BorrarAutor")]
        //endpoint: api/autores/9
        public ActionResult<Autor> Delete(int id)
        {
            var autor = context.Autores.FirstOrDefault(x => x.Id == id);
            if (autor == null)
            {
                //si el recurso no existe en la base de datos informamos el error
                return NotFound();
            }

            context.Autores.Remove(autor);
            context.SaveChanges();
            //retornamos el recurso borrado
            return autor;
        }

        [HttpDelete("id")]
        //endpoint: api/autores/9
        public async Task<ActionResult<Autor>> DeleteDTO(int id)
        {
            //traemos solo el campo ID para que sea mas performante
            var autorId = context.Autores.Select(x => x.Id).FirstOrDefault(x => x == id);
            if (autorId == default(int))
            {
                //si el recurso no existe en la base de datos informamos el error
                return NotFound();
            }
            //con solo pasarle el ID es suficiente, no hace falta pasarle el recurso completo
            context.Autores.Remove(new Autor {Id= autorId });
            await context.SaveChangesAsync();
            //retornamos nada
            return NoContent();
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult> Patch2( int id, [FromBody] JsonPatchDocument<AutorCreacionDTO> patchDocument)
        {
            if (patchDocument == null)
            {
                return BadRequest();
            }
            var autorDeLaDB = await context.Autores.FirstOrDefaultAsync(x => x.Id == id);
            if (autorDeLaDB == null)
            {
                return NotFound();
            }
            //mapeamos de autorDeLaDB a AutorCreacionDTO
            var autorDTO = _mapper.Map<AutorCreacionDTO>(autorDeLaDB);
            //vamos a aplicar el patch sobre el recurso obtenido en la base de datos
            patchDocument.ApplyTo(autorDTO, ModelState);
            //mapeamos desde autorDTO a autorDeLaDB
            _mapper.Map(autorDTO, autorDeLaDB);            
            var isValid = TryValidateModel(autorDeLaDB);
            if(!isValid){
                return BadRequest(ModelState);
            };
            await context.SaveChangesAsync();
            return NoContent();

        }
    }
}
